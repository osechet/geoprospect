geoprospect
===

To start the application:
```
docker-compose up --build
```

To start detached:
```
docker-compose up --build -d
```

The first time the application is started:
```
docker-compose run web python manage.py makemigrations
docker-compose run web python manage.py migrate
docker-compose run web python manage.py createsuperuser
```

To stop:
```
docker-compose down
```

Then open a browser on http://localhost:8000/admin to create new data and http://localhost:8000 to visualize data.

To view the logs:
```
docker-compose logs
```
