#!/bin/bash -eu

delay="$1"
shift
shift
cmd="$@"

sleep $delay
exec $cmd
