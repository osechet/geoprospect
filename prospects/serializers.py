from rest_framework import serializers
from rest_framework_gis.serializers import GeoModelSerializer

from .models import Customer, Product, Prospect


class CustomerSerializer(GeoModelSerializer):
    class Meta:
        model = Customer
        fields = ['name']


class ProductSerializer(GeoModelSerializer):
    class Meta:
        model = Product
        fields = ['name', 'version', 'family']


class ProspectSerializer(GeoModelSerializer):
    products = serializers.StringRelatedField(many=True)
    customer = serializers.StringRelatedField(many=False)
    engineers = serializers.StringRelatedField(many=True)

    class Meta:
        model = Prospect
        fields = ['name', 'location', 'products', 'customer', 'description',
                  'engineers', 'start_date', 'end_date']
