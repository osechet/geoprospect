from django.contrib.gis import admin
from django.contrib.gis.admin import OSMGeoAdmin
from django.contrib.admin import ModelAdmin

from .models import Customer, Family, FSE, Product, Prospect


@admin.register(Family)
class FamilyAdmin(ModelAdmin):
    pass


@admin.register(FSE)
class FSEAdmin(ModelAdmin):
    list_display = ('name', 'first_name')


@admin.register(Customer)
class CustomerAdmin(ModelAdmin):
    pass


@admin.register(Product)
class ProductAdmin(ModelAdmin):
    list_display = ('name', 'version')


@admin.register(Prospect)
class ProspectAdmin(OSMGeoAdmin):
    list_display = ('name', 'location')
