
from rest_framework import viewsets

from .models import Customer, Product, Prospect
from .serializers import CustomerSerializer, ProductSerializer, ProspectSerializer


class CustomerViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer



class ProspectViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Prospect.objects.all()
    serializer_class = ProspectSerializer
