from django.contrib.gis.db import models


class Family(models.Model):
    class Meta:
        verbose_name_plural = "Families"

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class FSE(models.Model):
    class Meta:
        verbose_name = "FSE"
        verbose_name_plural = "FSEs"

    name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)

    def __str__(self):
        return "{} {}".format(self.name, self.first_name)


class Product(models.Model):
    name = models.CharField(max_length=100)
    version = models.CharField(max_length=50)
    family = models.ForeignKey(Family, on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} v{}".format(self.name, self.version)


class Customer(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Prospect(models.Model):
    name = models.CharField(max_length=100)
    location = models.PointField()
    products = models.ManyToManyField(Product)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)
    engineers = models.ManyToManyField(FSE)
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
