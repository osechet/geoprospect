
function makeRequest(method, url) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send();
    });
}

function createPopup(prospect) {
    let popup = `<b>${prospect.name}</b><br>`;
    popup += '<hr>';
    popup += `Started: ${prospect.start_date}<br>`;
    if (prospect.end_date) {
        popup += `Ended: ${prospect.end_date}<br>`;
    }
    popup += '<hr>';
    popup += `Customer: ${prospect.customer}<br>`;
    popup += '<hr>';
    if (prospect.products.length == 1) {
        popup += `Product: ${prospect.products[0]}<br>`;
    } else {
        popup += 'Products: <ul class="popup-ul">';
        for (product of prospect.products) {
            popup += `<li>${product}</li>`;
        }
        popup += '</ul>';
    }
    popup += '<hr>';
    if (prospect.engineers.length == 1) {
        popup += `Team: ${prospect.engineers[0]}<br>`;
    } else {
        popup += 'Team: <ul class="popup-ul">';
        for (engineer of prospect.engineers) {
            popup += `<li>${engineer}</li>`;
        }
        popup += '</ul>';
    }
    return popup;
}

let map = L.map('mapid').setView([47.2681941, -1.4897588], 7);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '© OpenStreetMap contributors',
    maxZoom: 19,
}).addTo(map);

let adminControl = L.Control.extend({
    options: {
        position: 'topright'
    },
    onAdd: function(map) {
        let container = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');

        container.style.backgroundColor = 'white';
        container.style.width = '30px';
        container.style.height = '30px';

        let img = L.DomUtil.create('img');
        img.src = '/static/images/build-24px.svg';
        img.style.width = '20px';
        img.style.margin = '5px';
        container.appendChild(img);

        container.onclick = function(){
            let win = window.open("/admin", '_blank');
            win.focus();
        }
        return container;
    }
});

map.addControl(new adminControl());


makeRequest('GET', '/api/prospects')
    .then(function (prospects) {
        let imagePath = '{% static "images/picto_land.png" %}';
        for (prospect of prospects.results) {
            let icon = L.icon({
                iconUrl: imagePath,
                iconAnchor: [22, 14],
            });
            let lat = prospect.location.coordinates[1];
            let lng = prospect.location.coordinates[0];
            let marker = L.marker([lat, lng], {
                // icon: icon,
            }).addTo(map);
            marker.bindPopup(createPopup(prospect));
        }
    })
    .catch(function (err) {
        console.error('Augh, there was an error!', err);
    });
