FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN apt-get update && apt-get install -y gdal-bin libgdal-dev python3-gdal \
    binutils libproj-dev \
    postgresql-client
RUN pip install -r requirements.txt
COPY . /code/
